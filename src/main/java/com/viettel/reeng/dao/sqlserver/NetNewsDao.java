package com.viettel.reeng.dao.sqlserver;
import com.viettel.reeng.dao.sqlserver.entity.Categories;
import com.viettel.reeng.entity.News;
import com.viettel.reeng.entity.Quote;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

@Repository
@Service
public class NetNewsDao {
    @Autowired
    @Qualifier("jdbcSqlServerNetNews")
    private NamedParameterJdbcTemplate jdbcSqlServerTemplate;

    private static final Logger logger = LogManager.getLogger(NetNewsDao.class);

    public List<Categories> getCategories() {
        try {
            List<Integer> list = Arrays.asList(1, 3, 5, 6, 7, 10, 16, 27, 28, 29, 32, 33, 36, 37, 43, 44);
            String sql = " SELECT TOP 30 ID,PID,vt,Name FROM Categories where ID in (:list)";
            MapSqlParameterSource namedParameters = new MapSqlParameterSource("list", list);
            return jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<Categories>() {
                @Override
                public Categories mapRow(ResultSet rs, int rowNum) throws SQLException {
                    Categories category = new Categories();
                    category.setId(Integer.parseInt(rs.getString("ID")));
                    category.setPid(rs.getInt("PID"));
                    category.setVt(rs.getInt("vt"));
                    category.setName(rs.getString("Name"));
                    return category;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    //<editor-fold desc="All functions from SQL Server database">
    public Integer getReads(Integer mid) {
        try {
            String sql = " select top(1) Hit from Mess_Cate_Temp where mid = :mid ";


            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid);

            List<Integer> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getInt("Hit");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return 0;
    }

    public String getMessMedia(int mid, int type) {
        String src = "";
        try {
            String sql = " select src from Mess_Media where mid = :mid and type = :type ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid)
                    .addValue("type", type);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("src");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return src;
    }

    public String getMessMediaBK(int mid, int type) {

        try {
            String sql = " select src from Mess_Media_201503190 where mid = :mid and type = :type ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid)
                    .addValue("type", type);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("src");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getLeadMediaMessages(int mid) {

        try {
            String sql = " select LeadImage from Messages where ID = :mid  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("LeadImage");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getLeadMediaMessRadio(int mid) {

        try {
            String sql = " select link from Mess_Radio where mid = :mid  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("link");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getCategoryName(int pid) {

        try {
            String sql = " select Name from Categories where id = :pid  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("pid", pid);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("Name");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getSourceNameApp(String sid) {

        try {
            String sql = " select  Name1 + ',' + image as SourceName  from Sources where id = :sid  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("sid", sid);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("SourceName");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public String getIconTimer(int id) {
        try {

            String sql = " select  Timer from Mess_Media where type = 12 and mid = :id  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("Timer");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    public Integer getIconAppMessIcon(int mid) {
        try {

            String sql = " select  [type] as image from Mess_Icon where mid = :mid  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid);

            List<Integer> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return Integer.parseInt(rs.getString("image")) ;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return 0;
    }

    public Integer getIconAppMessIconBackup(int mid) {
        try {

            String sql = " select  [type] as image from Mess_Icon_Backup where mid = :mid  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("mid", mid);

            List<Integer> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return Integer.parseInt(rs.getString("image")) ;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return 0;
    }

    public Integer getMidMessCateTemp(int id,int order) {
        try {

            String sql = " select top(:order)  mid from Mess_Cate_Temp where pid = 194 and cid = :id and status = 2 order by DatePub desc  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("id", id)
                                                    .addValue("order",order);

            List<Integer> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return Integer.parseInt(rs.getString("mid")) ;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(items.size()-1);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return 0;
    }

    public String getLatestTitleOfCate(int cid) {
        try {

            String sql = " SELECT TOP (1)  Title FROM dbo.Mess_Cate WHERE cid = :cid AND Status =2 AND DatePub<= GETDATE() ORDER BY DatePub DESC  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("cid", cid);

            List<String> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    return rs.getString("Title");
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }

        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return "";
    }

    //</editor-fold>

    //<editor-fold desc="getNewsHomeV2 ">
    public News getTopNews() {
        try {
            String sql = " SELECT TOP (1) id1 as id, ABS(CAST(NEWID() AS binary(6)) %30) + 1  as CountLike, "
                    + " s.DatePub,cid,pid,s.title, s.lead,s.sid from Status s  "
                    + " JOIN dbo.Messages m ON s.id1 = m.ID where  type = 1 and s.datepub < getdate() AND m.Status = 2 ORDER BY  [Order] ASC ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setPid(rs.getInt("pid"));
                    item.setCid(rs.getInt("cid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopThreeFocusNew() {
        try {
            String sql = " SELECT TOP (1) id1 as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " cid,s.pid,s.title,s.lead,s.DatePub,s.sid from Status s "
                    + " JOIN dbo.Messages m ON s.id1 = m.ID where  id1 IS NOT NULL AND m.Status = 2 and "
                    + " type = 29  and s.datepub < getdate() and  (DATEPART ( hour , getdate() ) > 17 or DATEPART ( hour , getdate() ) < 7) "
                    + " union SELECT TOP (1) id1 as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " cid,s.pid,s.title,s.leads.DatePub,s.sid from Status s "
                    + " JOIN dbo.Messages m ON s.id1 = m.ID where  id1 IS NOT NULL AND m.Status = 2 and type = 19  and s.datepub < getdate() "
                    + " union SELECT TOP (1) id1 as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " cid,s.pid,s.title,s.lead,s.DatePub,s.sid from Status s "
                    + " JOIN dbo.Messages m ON s.id1 = m.ID where  id1 IS NOT NULL AND m.Status = 2 and type = 25  and s.datepub < getdate() ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {
                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopTwentyFiveFocusNew() {
        try {
            String sql = " SELECT TOP (25) ROW_NUMBER() OVER(ORDER BY [Order] ASC) + 4 AS rownumber , id1 as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " cid,s.pid,s.title,s.lead,s.DatePub,s.sid  from Status s "
                    + " JOIN dbo.Messages m ON s.id1 = m.ID  where  id1 IS NOT NULL AND m.Status = 2 and "
                    + " type = 24  and s.datepub < getdate() AND pid NOT IN (151)  order by rownumber ";

            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {
                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopEventNews() {
        try {
            String sql = " SELECT top(3) ID,PID,Name FROM Categories where Status = 2 and PID = 194 and isevent = 1 order by [order] asc  ";

            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("ID"));
                    item.setPid(rs.getInt("PID"));
                    item.setTitle(rs.getString("Name"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopInterestNews() {
        try {
            String sql = " SELECT TOP (5) mid as id ,ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + "LinkRadio  as link,LeadImage as LeadImage169, cid,pid,title, lead,LeadImage,DatePub,sid "
                    + "from Mess_CateTem where  datepub < GETDATE() and status = 2 and  DatePub > DATEADD(HOUR,-6,GETDATE()) "
                    + "AND pid > 0  order by hit desc ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setMedia_url(rs.getString("link"));
                    item.setImage169(rs.getString("LeadImage169"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setImage(rs.getString("LeadImage"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public Quote getQuoteFromBanners() {
        try {
            String sql = " select top(1) Link,Name,[Desc] as Description  from Banners where type = 66 and status = 2 order by id desc ";

            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<Quote> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<Quote>() {
                @Override
                public Quote mapRow(ResultSet rs, int rowNum) throws SQLException {

                    Quote item = new Quote();
                    item.setLink(rs.getString("Link"));
                    item.setName(rs.getString("Name"));
                    item.setPoster(rs.getString("Description"));
                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items.get(0);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopQuoteNews() {
        try {
            Quote quote = getQuoteFromBanners();
            String link = quote.getLink();
            Integer mid = Integer.parseInt(link.substring(link.indexOf(".html") -7,link.indexOf(".html")));

            String sql =  "SELECT TOP (1) mid as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " LinkRadio  as link,LeadImage as LeadImage169, cid,pid,:Name as  Quote ,title, lead, "
                    + " LeadImage,DatePub,sid, :Poster as poster  from Mess_Cate_Temp "
                    + " where mid =  :mid and pid in (151,560,1,3,5,6,7,10,149,150,170,171,29,312,135,528,542,728,757,758,759,760,761,763,764,773,166,1488,313) ";


            SqlParameterSource namedParameters = new MapSqlParameterSource("Name",quote.getName())
                                                                .addValue("Poster",quote.getPoster())
                                                                .addValue("mid",mid);

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setMedia_url(rs.getString("link"));
                    item.setImage169(rs.getString("LeadImage169"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setQuote(rs.getString("Quote"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setImage(rs.getString("LeadImage"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));
                    item.setPoster(rs.getString("poster"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopVideoNews() {
        try {
            String sql = " SELECT TOP (1)id1 as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " cid,status.pid,title, lead,DatePub,sid  from Status "
                    + " where  type = 5 and pid = 135  and Status.datepub < getdate()  ORDER BY  [Order] ASC  ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopFiveVideoNews() {
        try {
            String sql = " SELECT TOP(5) mid as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike,LinkMp4  as link, "
                    + " LeadImage as LeadImage169, cid,pid,title, lead,LeadImage, DatePub,sid "
                    + " FROM    Mess_Cate_Temp   where status = 2 and pid = 135 and Mess_Cate_Temp.DatePub < getdate() and mid != (select top(1) id1 from Status where "
                    + " type = 5 and pid = 135  and Status.datepub < getdate()  ORDER BY  [Order] ASC ) order by  Mess_Cate_Temp.DatePub desc  ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setMedia_url(rs.getString("link"));
                    item.setImage169(rs.getString("LeadImage169"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setImage(rs.getString("LeadImage"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopRadioNews() {
        try {
            String sql = " SELECT TOP (1) id1 as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, "
                    + " cid,status.pid,title, lead,DatePub,sid   from Status "
                    + " where type =5 and pid = 151 and  datepub < getdate()  ORDER BY status.[order] asc  ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }

    public List<News> getTopFourRadioNews() {
        try {
            String sql = " SELECT TOP (4) mid as id , ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike, link, "
                    + " cid,pid,title,lead,DatePub,sid from "
                    + " Mess_Radio  where   pid = 151 and  datepub < getdate() and status = 2 and mid != (select top(1) id1 from Status where type =5 and pid = 151 and "
                    + " datepub < getdate()  ORDER BY status.[order] asc) ORDER BY Mess_Radio.DatePub desc  ";


            SqlParameterSource namedParameters = new MapSqlParameterSource();

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {

                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setMedia_url(rs.getString("link"));
                    item.setCid(rs.getInt("cid"));
                    item.setPid(rs.getInt("pid"));
                    item.setTitle(rs.getString("title"));
                    item.setShapo(rs.getString("lead"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }
    //</editor-fold>


    //<editor-fold desc="getListCateRelateEvent ">
    public List<News> getListCateRelateEvent(int pid,int cid,int id,int page) {
        try {
            int start = (page-1) * 10;
            int end = page  * 10;
            String sql = " select  mid as id,pid,cid,Title, Lead,"
                    + "  DatePub, '0' as SLComment,ABS(CAST(NEWID() AS binary(6)) %30) + 1 as CountLike,2 as type, "
                    + " hit * 121 as hit ,'0' cateevent,sid, "
                    + " dbo.DTtoUnixTS(datepub) as unixTime,ROW_NUMBER() OVER(ORDER BY DatePub DESC) as rownumber "
                    + " FROM  Mess_Cate_Temp where   datepub < getdate() AND Status=2   and pid = :pid and cid = :cid and mid != :id "
                    + "  ORDER BY DatePub DESC,rownumber asc ";

            SqlParameterSource namedParameters = new MapSqlParameterSource("pid",pid)
                    .addValue("cid",cid)
                    .addValue("id",id)
                    .addValue("page",page);

            List<News> items = jdbcSqlServerTemplate.query(sql, namedParameters, new RowMapper<News>() {
                @Override
                public News mapRow(ResultSet rs, int rowNum) throws SQLException {
                    News item = new News();
                    item.setID(rs.getInt("id"));
                    item.setPid(rs.getInt("pid"));
                    item.setCid(rs.getInt("cid"));
                    item.setShapo(rs.getString("Lead"));
                    item.setTitle(rs.getString("Title"));
                    item.setLike(rs.getInt("CountLike"));
                    item.setDatePub(rs.getString("DatePub"));
                    item.setSid(rs.getString("sid"));
                    item.setReads(rs.getInt("hit"));

                    return item;
                }
            });
            if (CollectionUtils.isNotEmpty(items)) {
                return items;
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }
        return null;
    }
    //</editor-fold>
}
