package com.viettel.reeng.dao.mysql.entity;

public class AccumulateGifChange {

	private int id;
	private String title;
	private String point_desc;
	private int point;
	private int type;
	private int gif_type;
	private String content_fake_mo;
	private String confirm_fake_mo;
	private String title_fake_mo;
	private String url;
	private String btn_ok;
	private String btn_cancel;
	private String btn_confirm_ok;
	private String btn_confirm_cancel;
	private String command;
	private int androidRevision;
	private int iosRevision;
	private String label;
	private String icon;

	public AccumulateGifChange() {

	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public int getGif_type() {
		return gif_type;
	}

	public void setGif_type(int gif_type) {
		this.gif_type = gif_type;
	}

	public String getContent_fake_mo() {
		return content_fake_mo;
	}

	public void setContent_fake_mo(String content_fake_mo) {
		this.content_fake_mo = content_fake_mo;
	}

	public String getConfirm_fake_mo() {
		return confirm_fake_mo;
	}

	public void setConfirm_fake_mo(String confirm_fake_mo) {
		this.confirm_fake_mo = confirm_fake_mo;
	}

	public String getTitle_fake_mo() {
		return title_fake_mo;
	}

	public void setTitle_fake_mo(String title_fake_mo) {
		this.title_fake_mo = title_fake_mo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getBtn_ok() {
		return btn_ok;
	}

	public void setBtn_ok(String btn_ok) {
		this.btn_ok = btn_ok;
	}

	public String getBtn_cancel() {
		return btn_cancel;
	}

	public void setBtn_cancel(String btn_cancel) {
		this.btn_cancel = btn_cancel;
	}

	public String getBtn_confirm_ok() {
		return btn_confirm_ok;
	}

	public void setBtn_confirm_ok(String btn_confirm_ok) {
		this.btn_confirm_ok = btn_confirm_ok;
	}

	public String getBtn_confirm_cancel() {
		return btn_confirm_cancel;
	}

	public void setBtn_confirm_cancel(String btn_confirm_cancel) {
		this.btn_confirm_cancel = btn_confirm_cancel;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPoint_desc() {
		return point_desc;
	}

	public void setPoint_desc(String point_desc) {
		this.point_desc = point_desc;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public int getAndroidRevision() {
		return androidRevision;
	}

	public void setAndroidRevision(int androidRevision) {
		this.androidRevision = androidRevision;
	}

	public int getIosRevision() {
		return iosRevision;
	}

	public void setIosRevision(int iosRevision) {
		this.iosRevision = iosRevision;
	}

}
