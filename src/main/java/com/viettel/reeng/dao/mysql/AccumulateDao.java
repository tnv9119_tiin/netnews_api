package com.viettel.reeng.dao.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import com.viettel.reeng.config.Configuration;
import com.viettel.reeng.dao.mysql.entity.AccumulateGifChange;
import com.viettel.reeng.service.RedisService;
import com.viettel.reeng.utils.ReengUtils;

@Repository
public class AccumulateDao {
	private static final Logger logger = LogManager.getLogger(AccumulateDao.class);

}
