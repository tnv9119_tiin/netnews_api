package com.viettel.reeng.controller;
import com.google.gson.Gson;
import com.viettel.reeng.config.ReengCodeDesc;
import com.viettel.reeng.entity.News;
import com.viettel.reeng.json.ResponseData;
import com.viettel.reeng.json.ResponseObject;
import com.viettel.reeng.service.NetNewsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping("/category")
@Controller
public class CategoryController {
    private static final Logger logger = LogManager.getLogger(CategoryController.class);
    @Autowired
    private NetNewsService netNewsService;

    @RequestMapping(value = "/getTopNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getTopNews(HttpServletRequest request) {
        try {
            String result = "";
            News news = netNewsService.getTopNews();
            if (news != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNews(news);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }

    @RequestMapping(value = "/getFocusNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getFocusNews(@RequestParam("isVip") int isVip,HttpServletRequest request) {
        try {
            String result = "";
            List<News> news = netNewsService.getFocusNews(isVip);
            if (news != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(news);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }


    @RequestMapping(value = "/getTopEventNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getTopEventNews(HttpServletRequest request) {
        try {
            String result = "";
            List<News> items = netNewsService.getTopEventNews();
            if (items != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(items);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }

    @RequestMapping(value = "/getTopInterestNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getTopInterestNews(HttpServletRequest request) {
        try {
            String result = "";
            List<News> items = netNewsService.getTopInterestNews();
            if (items != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(items);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }

    @RequestMapping(value = "/getTopQuoteNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getTopQuoteNews(HttpServletRequest request) {
        try {
            String result = "";
            List<News> items = netNewsService.getTopQuoteNews();
            if (items != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(items);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }


    @RequestMapping(value = "/getVideoNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getVideoNews(@RequestParam("isVip") int isVip,HttpServletRequest request) {
        try {
            String result = "";
            List<News> news = netNewsService.getVideoNews(isVip);
            if (news != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(news);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }

    @RequestMapping(value = "/getRadioNews", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getRadioNews(@RequestParam("isVip") int isVip,HttpServletRequest request) {
        try {
            String result = "";
            List<News> news = netNewsService.getRadioNews(isVip);
            if (news != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(news);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }

    @RequestMapping(value = "/getListCateRelateEvent", method = RequestMethod.GET, produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String getListCateRelateEvent(HttpServletRequest request) {
        try {
            String result = "";
            List<News> news = netNewsService.getListCateRelateEvent(194,2033,1809695,1);
            if (news != null) {
                ResponseObject res = new ResponseObject(ReengCodeDesc.ReengCode.SUCCESSFUL);
                ResponseData resData = new ResponseData();
                resData.setNewsList(news);
                res.setData(resData);
                result = new Gson().toJson(res);
            }
            return result;
        } catch (Exception e) {
            logger.info("Exception:" + e.getMessage(), e);
            return ReengCodeDesc.ReengCode.ERROR_INTERNAL.getOtpJsonString();
        }
    }


}
