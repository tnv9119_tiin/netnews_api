package com.viettel.reeng.utils;

import com.viettel.reeng.dao.sqlserver.NetNewsDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public class NetNewsUtilsDB {
    private static final Logger logger = LogManager.getLogger(NetNewsUtilsDB.class);

    @Autowired
    private NetNewsDao netNewsDao;

    public int getReads(int mid) {
        int reads = 0;
        try {

            int hit = netNewsDao.getReads(mid);
            reads = hit * 121;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return reads;
    }

    public String getMedia(Integer mid, Integer type) {
        String leadMedia = "";
        try {

            if (mid > 2000000) {
                if (type == 3) {
                    leadMedia = netNewsDao.getMessMedia(mid, 7);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia.replace(".jpg", "wap_75.jpg");
                        leadMedia = leadMedia.replace(".gif", "wap_75.gif");
                        leadMedia = leadMedia.replace(".png", "wap_75.png");
                    } else {
                        leadMedia = netNewsDao.getMessMedia(mid, type);
                    }
                } else if (type == 4) {
                    leadMedia = netNewsDao.getMessMedia(mid, 0);
                    if (!leadMedia.isEmpty()) {
                        if (leadMedia.indexOf("http://media2.sieuhai.tv") != -1 && leadMedia.indexOf("http://mcvideo") != -1) {
                            leadMedia = leadMedia;
                        } else {
                            leadMedia = leadMedia.replace(".jpg", "wap_320.jpg");
                            leadMedia = leadMedia.replace(".gif", "wap_320.gif");
                            leadMedia = leadMedia.replace(".png", "wap_320.png");
                        }
                    } else {
                        leadMedia = netNewsDao.getMessMedia(mid, type);
                    }
                } else if (type == 5) {
                    leadMedia = netNewsDao.getMessMedia(mid, 5);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = netNewsDao.getMessMedia(mid, 1);
                    }
                } else if (type == 12) {
                    leadMedia = netNewsDao.getMessMedia(mid, 12);
                } else {
                    leadMedia = netNewsDao.getMessMedia(mid, type);
                    if (leadMedia.isEmpty()) {
                        if (type == 6) {
                            leadMedia = netNewsDao.getMessMedia(mid, 6);
                        } else if (type == 7) {
                            leadMedia = netNewsDao.getMessMedia(mid, 3);
                        }
                    }
                }
            }
            //Lay du lieu bang backup
            else {
                if (type == 12) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 12);
                    if (leadMedia.isEmpty())
                        leadMedia = netNewsDao.getMessMedia(mid, 12);
                } else if (type == 3) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 7);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia.replace(".jpg", "wap_75.jpg");
                        leadMedia = leadMedia.replace(".gif", "wap_75.gif");
                        leadMedia = leadMedia.replace(".png", "wap_75.png");
                    } else {
                        leadMedia = netNewsDao.getMessMediaBK(mid, type);
                    }
                } else if (type == 4) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 6);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia.replace(".jpg", "wap_320.jpg");
                        leadMedia = leadMedia.replace(".gif", "wap_320.gif");
                        leadMedia = leadMedia.replace(".png", "wap_320.png");
                    } else {
                        leadMedia = netNewsDao.getMessMediaBK(mid, type);
                    }
                } else if (type == 5) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 5);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = netNewsDao.getMessMediaBK(mid, 1);
                    }
                } else {
                    leadMedia = netNewsDao.getMessMediaBK(mid, type);
                    if (leadMedia.isEmpty()) {
                        leadMedia = netNewsDao.getMessMediaBK(mid, type);
                        if (type == 6)
                            leadMedia = netNewsDao.getMessMediaBK(mid, 6);
                        else if (type == 7) {
                            leadMedia = netNewsDao.getMessMediaBK(mid, 3);
                        }
                    }
                }
            }
            if (leadMedia == "" || leadMedia.isEmpty()) {
                if (mid > 2000000) {
                    leadMedia = netNewsDao.getMessMedia(mid, 0);
                } else
                    leadMedia = netNewsDao.getMessMediaBK(mid, 0);
            }
            if (type == 12 && leadMedia.indexOf(".jpg") != -1) {
                leadMedia = netNewsDao.getLeadMediaMessRadio(mid);
            }
            if (leadMedia == "" || leadMedia.isEmpty()) {
                leadMedia = netNewsDao.getLeadMediaMessages(mid);
            }

            if (!leadMedia.isEmpty())
                leadMedia = leadMedia.replace("\"", "/");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return leadMedia;
    }

    public String getMediaNews(Integer mid, Integer type) {
        String leadMedia = "";
        try {

            if (mid > 2000000) {
                if (type == 3) {
                    leadMedia = netNewsDao.getMessMedia(mid, 7);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia.replace(".jpg", "wap_75.jpg");
                        leadMedia = leadMedia.replace(".gif", "wap_75.gif");
                        leadMedia = leadMedia.replace(".png", "wap_75.png");
                    } else {
                        leadMedia = netNewsDao.getMessMedia(mid, type);
                    }
                } else if (type == 4) {
                    leadMedia = netNewsDao.getMessMedia(mid, 0);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia;
                    } else {
                        leadMedia = netNewsDao.getMessMedia(mid, type);
                    }
                } else if (type == 5) {
                    leadMedia = netNewsDao.getMessMedia(mid, 5);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = netNewsDao.getMessMedia(mid, 1);
                    }
                } else if (type == 12) {
                    leadMedia = netNewsDao.getMessMedia(mid, 12);
                } else {
                    leadMedia = netNewsDao.getMessMedia(mid, type);
                    if (leadMedia.isEmpty()) {
                        if (type == 6) {
                            leadMedia = netNewsDao.getMessMedia(mid, 6);
                        } else if (type == 7) {
                            leadMedia = netNewsDao.getMessMedia(mid, 3);
                        }
                    }
                }
            }
            //Lay du lieu bang backup
            else {
                if (type == 12) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 12);
                    if (leadMedia.isEmpty())
                        leadMedia = netNewsDao.getMessMedia(mid, 12);
                } else if (type == 3) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 7);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia.replace(".jpg", "wap_75.jpg");
                        leadMedia = leadMedia.replace(".gif", "wap_75.gif");
                        leadMedia = leadMedia.replace(".png", "wap_75.png");
                    } else {
                        leadMedia = netNewsDao.getMessMediaBK(mid, type);
                    }
                } else if (type == 4) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 6);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = leadMedia.replace(".jpg", "wap_320.jpg");
                        leadMedia = leadMedia.replace(".gif", "wap_320.gif");
                        leadMedia = leadMedia.replace(".png", "wap_320.png");
                    } else {
                        leadMedia = netNewsDao.getMessMediaBK(mid, type);
                    }
                } else if (type == 5) {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 5);
                    if (!leadMedia.isEmpty()) {
                        leadMedia = netNewsDao.getMessMediaBK(mid, 1);
                    }
                } else {
                    leadMedia = netNewsDao.getMessMediaBK(mid, type);
                    if (leadMedia.isEmpty()) {
                        leadMedia = netNewsDao.getMessMediaBK(mid, type);
                        if (type == 6)
                            leadMedia = netNewsDao.getMessMediaBK(mid, 6);
                        else if (type == 7) {
                            leadMedia = netNewsDao.getMessMediaBK(mid, 3);
                        }
                    }
                }
            }

            if (leadMedia == "" || leadMedia.isEmpty()) {
                if (mid > 1481543) {
                    leadMedia = netNewsDao.getMessMedia(mid, 0);
                } else {
                    leadMedia = netNewsDao.getMessMediaBK(mid, 0);
                }
            }

            if (leadMedia == "" || leadMedia.isEmpty()) {
                leadMedia = netNewsDao.getLeadMediaMessages(mid);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return leadMedia;
    }

    public String formatDate3GApp(Date inputDate) {
        int hour = 0;
        int day,minute;
        try {
            Date now = new Date();
            day = inputDate.getDay();
            if( now.getDay() - day >= 1)
            {
                SimpleDateFormat dateOnly = new SimpleDateFormat("dd/MM/yyyy");
                return dateOnly.format(inputDate.getTime());
            }
            else
            {
                minute = inputDate.getMinutes();
                if(minute == 0)
                {
                    return String.valueOf(now.getSeconds() - inputDate.getSeconds()) + " giây trước";
                }
                else if(minute < 60)
                {
                    return  String.valueOf(minute) + " phút trước";
                }
                else if(minute == 60)
                {
                    return  "1 giờ trước";
                }
                hour = inputDate.getHours();
                if(hour >= 1 && hour <= 6)
                {
                    minute = minute - hour * 60;
                    if(minute < 0)
                    {
                        hour = hour -1;
                        minute = minute + 60;
                        return  hour + "h" + minute + " phút trước";
                    }
                    else
                    {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
                        String sDate = dateFormat.format(inputDate);
                        return sDate.substring(0,5);
                    }
                }
                return (now.getMinutes() - inputDate.getMinutes()) % 60 + " phút trước";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return String.valueOf(hour);
    }

    public String formatTimerRadio(Date inputDate) {
        String result = "";
        int hour,minute;
        try {
            hour = inputDate.getHours();
            minute = inputDate.getMinutes();
            if(hour < 10)
            {
                result = '0' + String.valueOf(hour);
            }
            else {
                result = String.valueOf(hour);
            }

            if(minute < 10)
            {
                result = result + ":0" + String.valueOf(minute);
            }
            else
            {
                result = result + ":" + String.valueOf(minute);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getCategoryName(int pid) {
        String result = "";
        try {
            result = netNewsDao.getCategoryName(pid);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getSourceNameApp(String sid) {
        String result = "Đất Việt";
        try {
            result = netNewsDao.getSourceNameApp(sid);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getIconTimer(int id) {
        String result = "";
        try {
            if (id > 1481543)
                result = netNewsDao.getIconTimer(id);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public Integer getIconApp(int mid) {
        Integer result = 0;
        try {
            if (mid < 1073164)
                result = netNewsDao.getIconAppMessIconBackup(mid);
            else
                result = netNewsDao.getIconAppMessIcon(mid);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getMediaNetnews(int mid) {
        String result = "";
        try {
            if (mid > 2000000) {
                result = netNewsDao.getMessMedia(mid, 0);
                result.replace("\"", "/");
            } else {
                result = netNewsDao.getMessMediaBK(mid, 0);
            }

            if (result != "" && !result.isEmpty()) {
                if (result.indexOf("http://media2.sieuhai.tv") != -1 || result.indexOf("http://mcvideo") != -1) {
                    return result;
                } else {
                    result = result.replace(".jpg", "wap_240.jpg");
                    result = result.replace(".gif", "wap_240.gif");
                    result = result.replace(".png", "wap_240.png");
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getImageEvent(int id,int order) {
        String result = "";
        try {
            Integer mid = netNewsDao.getMidMessCateTemp(id,order);
            result = netNewsDao.getMessMedia(mid,0);
            if(result != "" && !result.isEmpty())
            {
                result = result.replace(".jpg","wap_240.jpg");
                result = result.replace(".gif","wap_240.gif");
                result = result.replace(".png","wap_240.png");
            }
            result = result.replace("\"","/");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public String getLatestTitleOfCate(int cid) {
        String result = "";
        try {
           result = netNewsDao.getLatestTitleOfCate(cid);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return result;
    }

    public  String getMediaIPAD(int mid,int type)
    {
        String result = "";

        try
        {
            if(mid > 1481543)
            {
                if(type == 3)
                {
                    result = netNewsDao.getMessMedia(mid,7);
                    if(result != "" && !result.isEmpty())
                    {
                        result = result.replace(".jpg","wap_75.jpg");
                        result = result.replace(".gif","wap_75.gif");
                        result = result.replace(".png","wap_75.png");
                    }
                    else {
                        result = netNewsDao.getMessMedia(mid,type);
                    }
                }
                else if(type == 4)
                {
                    result = netNewsDao.getMessMedia(mid,6);
                    if(result.isEmpty())
                        result = netNewsDao.getMessMedia(mid,type);
                }
                else
                {
                    if(type == 6)
                    {
                        result = netNewsDao.getMessMedia(mid,0);
                    }
                    else
                        result = netNewsDao.getMessMedia(mid,0);
                    if(result == "" || !result.isEmpty())
                    {
                        if(type == 7)
                            result = netNewsDao.getMessMedia(mid,3);
                    }
                }
            }
            if(result == "" || result.isEmpty())
            {
                if(mid > 1481543)
                {
                    result = netNewsDao.getMessMedia(mid,0);
                }
                else
                {
                    result = netNewsDao.getMessMediaBK(mid,0);
                }
            }
            if(result == "" || result.isEmpty())
            {
                result = netNewsDao.getLeadMediaMessages(mid);
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return  result;
    }

    public String dTtoUnixTS(Date datePub)
    {
        String result = "";
        Long diff = null;
        try{
            Long date = (datePub.getTime()/1000);
            if( date > 20380119)
            {
                diff = (20380119 - 19700101) + (date - 20380119);
            }
            else
            {
                diff = date - 19700101;
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return  String.valueOf(diff);
    }
}
