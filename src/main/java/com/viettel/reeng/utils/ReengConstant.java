package com.viettel.reeng.utils;

public class ReengConstant {
	/************************
	 * CONTACT CONST
	 *************************************/
	public static final int CONTACT_ACTION_SET = 1;
	public static final int CONTACT_ACTION_ADD = 2;
	public static final int CONTACT_ACTION_GET = 3;
	public static final int CONTACT_ACTION_DEL = 4;
	public static final int CONTACT_ACTION_REM_MEM = 4;
	public static final String CONTACTS = "contacts";
	public static final String MSISDN = "msisdn";

	public static final String CONTACT_MSISDN = "cmsisdn";
	public static final String CONTACT_NAME = "cname";
	public static final String CONTACT_STATUS = "status";
	public static final String CONTACT_LAVATAR = "lavatar";
	public static final String CONTACT_LONLINE = "lastOnline";
	public static final String CONTACT_OFFLINE_DURATION = "offDuration";
	public static final String CONTACT_LOFFLINE = "lastOffline";
	public static final String CONTACT_STATE = "state";
	public static final String CONTACT_GENDER = "gender";
	public static final String CONTACT_OPERATOR = "currOperator";
	public static final String CONTACT_DESKTOP = "desktop";
	public static final String CONTACT_PREKEYS = "e2e_prekey";
	public static final String CONTACT_CNAME = "cname";

	/************************
	 * USER CONST
	 ****************************************/
	public static final int USER_ACTIVE = 1;
	public static final int USER_DEACTIVE = 2;
	public static final int USERS_NOT_INSTALL = 0;
	public static final String JSON_USERSUB_DOMAIN = "domain";
	public static final String JSON_USERSUB_DESC = "content";
//	public static final String JSON_USERSUB_FILE_DOMAIN = "domain_file";
//	public static final String JSON_USERSUB_CHAT_DOMAIN = "domain_msg";

	// anhhn add change domain
	public static final String PROTOCOL_PREFIX = "http://";
	public static final String DOMAIN_FILE_V1 = "domain_file_v1";
	public static final String DOMAIN_IMG_V1 = "domain_img_v1";
	public static final String DOMAIN_KMUSIC = "domain_kmusic";
	public static final String DOMAIN_KMOVIES = "domain_kmovies";
	public static final String DOMAIN_NETNEWS = "domain_netnews";
	public static final String DOMAIN_TIIN = "domain_tiin";
	public static final String DOMAIN_MCVIDEO = "domain_mcvideo";
//	public static final String DOMAIN_KMUSIC_IMG = "domain_kmusic_img";
	public static final String DOMAIN_MSG = "domain_msg";
	public static final String DOMAIN_FILE = "domain_file";
	public static final String DOMAIN_IMG = "domain_img";
	public static final String DOMAIN_KSERVICE = "kservice";
	public static final String DOMAIN_KMEDIA = "kmedia";
	public static final String DOMAIN_KIMAGE = "kimage";
	public static final String DOMAIN_ON_MEDIA = "domain_on_media";
	public static final String DOMAIN_ON_MEDIA_V1 = "domain_on_media_v1";
	public static final String DOMAIN_KMUSIC_SEARCH = "domain_kmusic_search";
	public static final String DOMAIN_SSL_KEY = "ssl";

//	public static final String DOMAIN_KMUSIC_SERVICE = "domain_kmusic_service";
//	public static final String DOMAIN_KMUSIC_MEDIA = "domain_kmusic_media";

	public static final String TABLE = "users";
	public static final String USER_NAME = "username";
	public static final String NAME = "name";
	public static final String PASS = "password";
	public static final String PASS_EXP = "password_expired";
	public static final String GENDER = "gender";
	public static final String TOKEN = "token";
	public static final String TOKEN_EXP = "token_expired";
	public static final String ACTIVE = "active";
	public static final String STATUS = "status";
	public static final String LAST_AVATAR = "last_avatar";
	public static final String LAST_ONLINE = "last_online";
	public static final String LAST_OFFLINE = "last_offline";
	public static final String BIRTHDAY = "birthday";
	public static final String LAST_SEEN = "last_seen";
	public static final String NOTIFICATION_VIEW = "notification_view";

	public static final String UPLOAD_FILE = "file";
	public static final String UPLOAD_VOICEMAIL = "voicemail";
	public static final String UPLOAD_IMAGE = "image";
	public static final String UPLOAD_SHAREVIDEO = "sharevideo";

	public static final String SMS_OUT_FROM = "from";
	public static final String SMS_OUT_TO = "to";
	public static final String SMS_OUT_CONTENT = "content";
	public static final String SMS_OUT_SRC = "src";

	public static final int USER_REGISTER = 0;

	public static final int USER_STATE_ONLINE = 1;
	public static final int USER_STATE_OFFLINE = 0;

	public static final String JSON_KEY_CODE = "errorCode";
	public static final String JSON_SUCESSFULL_DESC = "OK";

	// user-subscription table
	public static final String USER_SUB_TABLE = "user_subscription";
	public static final String USER_SUB_MSISDN = "users_username";
	public static final String USER_SUB_SUB_ID = "sub_id";
	public static final String USER_SUB_STATUS = "status";
	public static final String USER_SUB_REGISTER_DATE = "sub_register_date";
	public static final String USER_SUB_SUB_END_DATE = "sub_end_date";
	public static final String USER_SUB_FREE_SMS = "free_sms";
	public static final String USER_SUB_FREE_SMS_EXPIRED_DATE = "free_sms_expired_date";
	public static final String USER_SUB_CHARGE_DATE = "charge_date";
	public static final String USER_SUB_CHARGE_STATE = "charge_state";

	public static final int USER_SUB_CHARGE_STATE_SUCCESS = 1;
	public static final int USER_SUB_CHARGE_STATE_FAIL = 2;

	public static final int USER_SUB_STATUS_ON = 1;
	public static final int USER_SUB_STATUS_OFF = 2;
	public static final int USER_SUB_STATUS_PENDING = 3;

	// subscription table
	// sub table
	public static final String SUB_TABLE = "subscription_package";
	public static final String SUB_ID = "id";
	public static final String SUB_ALIAS = "alias";
	public static final String SUB_DESC = "description";
	public static final String SUB_GUIDE = "guide";
	public static final String SUB_FEE = "fee";
	public static final String SUB_DURATION = "duration";// day
	public static final String SUB_DOMAIN = "domain";
	public static final String SUB_FILE_DOMAIN = "file_domain";
	public static final String SUB_CHAT_DOMAIN = "chat_domain";
	public static final String SUB_FREE_SMS = "free_sms";
	public static final String SUB_FREE_SMS_DURATION = "free_sms_duration";
	public static final String SUB_CHARGE_TYPE = "charging_type";
	public static final String SUB_DISABLE_OLD_SUB = "disable_old_sub";
	public static final String SUB_REGISTER_SUCCESSFUL_CONTENT = "successful_content";
	public static final String SUB_REGISTER_FAIL_CONTENT = "fail_content";

	public static final int SCHEDULED_THREADPOOL_SIZE_DEFAULT = 1;

	public static final int GOOGLE_SERVICE_TIMEOUT = 120000;
	public static final int GOOGLE_SERVICE_RETRIES = 5;

	public static final String DATE_ISO_FORMAT = "yyyy-MM-dd'T'HH:mm:ssX";

	public static final String LUCKYWHEEL_PAUSED_MESSAGE = "luckywheel.paused.message";
	public static final String LUCKYWHEEL_USER_NOTFOUND = "luckywheel.user.not.found";
	public static final String LUCKYWHEEL_USER_INFO_SUCCESS = "luckywheel.user.info.success";

	public static final String LUCKYWHEEL_SPIN_SUCCESS = "luckywheel.spin.success";
	public static final String LUCKYWHEEL_SPIN_EXCEEDED = "luckywheel.spin.exceeded";

	public static final String LUCKYWHEEL_TURN_ASK_OTHERMSISDN = "luckywheel.turn.ask.othermsisdn";
	public static final String LUCKYWHEEL_TURN_ASK_ATLEAST = "luckywheel.turn.ask.atleast";
	public static final String LUCKYWHEEL_TURN_ASK_SUCCESS = "luckywheel.turn.ask.success";
	public static final String LUCKYWHEEL_TURN_ACCEPT_SUCCESS = "luckywheel.turn.accept.success";
	public static final String LUCKYWHEEL_TURN_ACCEPT_REPEAT = "luckywheel.turn.accept.repeat";
	public static final String LUCKYWHEEL_TURN_ACCEPT_EXCEEDED = "luckywheel.turn.accept.exceeded";
	public static final String LUCKYWHEEL_TURN_ACCEPT_EXPIRED = "luckywheel.turn.accept.expired";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_ASKTURN = "luckywheel.chat.message.askingHelp";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_ACCEPT_LABEL = "luckywheel.chat.message.acceptHelp.label";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_ACCEPT_TURN = "luckywheel.chat.message.acceptHelp";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_ACCEPT_LINK = "luckywheel.chat.message.acceptHelp.link";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_POINT_ALIAS = "luckywheel.chat.message.oa.point.alias";
	public static final String LUCKYWHEEL_USER_DESC_10K = "luckywheel.user.desc.10k";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_INSTALLED = "luckywheel.chat.message.installed";
	public static final String LUCKYWHEEL_CHAT_SMS_ASKINGHELP = "luckywheel.chat.SMS.askingHelp";

	public static final String LUCKYWHEEL_TURN_ACCEPT_NOT_ACCEPT = "luckywheel.turn.accept.not.accept";
	public static final String LUCKYWHEEL_TURN_ASK_OTHERMSISDN_NOTVN = "luckywheel.turn.ask.othermsisdn.notvn";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_NOT_ACCEPTHELP_LABEL = "luckywheel.chat.message.not.acceptHelp.label";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_NOT_ACCEPTHELP_LINK = "luckywheel.chat.message.not.acceptHelp.link";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_SMS_ALIAS = "luckywheel.chat.message.oa.sms.alias";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_SECRET_INSTALL_SUCCESS = "luckywheel.chat.message.oa.secret.install.success";

	public static final String INVITE_LAO_PRE_MSISDN_NEW = "invite.lao.pre.msisdn.new";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_SECRET_INSTALL_ACTIVATE = "luckywheel.chat.message.oa.secret.install.activate";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_SECRET_INSTALL_DEVICE = "luckywheel.chat.message.oa.secret.install.device";
	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_SECRET_INSTALL_NOT_SUM = "luckywheel.chat.message.oa.secret.install.not.sum";

	public static final String LUCKYWHEEL_NOTE_POINT = "luckywheel.note.point";
	public static final String LUCKYWHEEL_NOTE_POINT_10K = "luckywheel.note.point.10k";
	public static final String LUCKYWHEEL_NOTE_10 = "luckywheel.note.10";
	public static final String LUCKYWHEEL_NOTE_INSTALL = "luckywheel.note.install";
	public static final String LUCKYWHEEL_NOTE_CARD = "luckywheel.note.card";

	public static final String LUCKYWHEEL_CHAT_MESSAGE_OA_NEWS_SUCCESS = "luckywheel.chat.message.oa.news.success";
	public static final String LUCKYWHEEL_NOTE_NEWS = "luckywheel.note.news";

	public static final String QR_SCAN_SUCCESS = "qr.scan.success";
	public static final String QR_SCAN_ERROR = "qr.scan.error";
	public static final String QR_SCAN_NOT_FORMAT = "qr.scan.not.format";
	public static final String QR_SCAN_THANKS_FORMAT = "qr.scan.thanks.format";
	public static final String QR_SCAN_NOT_STORE = "qr.scan.not.store";

	public static final String LUCKYWHEEL_NOTE_POINT_500 = "luckywheel.note.point.500";
	public static final String LUCKYWHEEL_NOTE_POINT_100 = "luckywheel.note.point.100";

	public static final String SPONSOR_OA_MISSION_LOGIN_KEENG_SUCCESS = "sponsor.oa.mission.login.keeng.success";
	public static final String SPONSOR_OA_MISSION_LOGIN_KEENG_ERROR = "sponsor.oa.mission.login.keeng.error";
	public static final String SPONSOR_OA_MISSION_LOGIN_KEENG_POINT = "sponsor.oa.mission.login.keeng.point";

	public static final String VIETNAM_COUNTRY_CODE = "VN";
	public static final String VIETNAM_LANGUAGE_CODE = "vi";

	public static final String LW_MYTEL_PAUSED_MESSAGE = "lw.mytel.paused.message";
	public static final String LW_MYTEL_USER_NOTFOUND = "lw.mytel.user.not.found";
	public static final String LW_MYTEL_USER_INFO_SUCCESS = "lw.mytel.user.info.success";

	public static final String LW_MYTEL_SPIN_SUCCESS = "lw.mytel.spin.success";
	public static final String LW_MYTEL_SPIN_EXCEEDED = "lw.mytel.spin.exceeded";

	public static final String LW_MYTEL_TURN_ASK_OTHERMSISDN = "lw.mytel.turn.ask.othermsisdn";
	public static final String LW_MYTEL_TURN_ASK_ATLEAST = "lw.mytel.turn.ask.atleast";
	public static final String LW_MYTEL_TURN_ASK_SUCCESS = "lw.mytel.turn.ask.success";
	public static final String LW_MYTEL_TURN_ACCEPT_SUCCESS = "lw.mytel.turn.accept.success";
	public static final String LW_MYTEL_TURN_ACCEPT_REPEAT = "lw.mytel.turn.accept.repeat";
	public static final String LW_MYTEL_TURN_ACCEPT_EXCEEDED = "lw.mytel.turn.accept.exceeded";
	public static final String LW_MYTEL_TURN_ACCEPT_EXPIRED = "lw.mytel.turn.accept.expired";
	public static final String LW_MYTEL_CHAT_MESSAGE_ASKTURN = "lw.mytel.chat.message.askingHelp";

	public static final String LW_MYTEL_CHAT_MESSAGE_ACCEPT_LABEL = "lw.mytel.chat.message.acceptHelp.label";
	public static final String LW_MYTEL_CHAT_MESSAGE_ACCEPT_TURN = "lw.mytel.chat.message.acceptHelp";
	public static final String LW_MYTEL_CHAT_MESSAGE_ACCEPT_LINK = "lw.mytel.chat.message.acceptHelp.link";

	public static final String LW_MYTEL_CHAT_MESSAGE_OA_POINT_ALIAS = "lw.mytel.chat.message.oa.point.alias";
	public static final String LW_MYTEL_USER_DESC_10K = "lw.mytel.user.desc.10k";

	public static final String LW_MYTEL_CHAT_MESSAGE_INSTALLED = "lw.mytel.chat.message.installed";
	public static final String LW_MYTEL_CHAT_SMS_ASKINGHELP = "lw.mytel.chat.SMS.askingHelp";

	public static final String LW_MYTEL_TURN_ACCEPT_NOT_ACCEPT = "lw.mytel.turn.accept.not.accept";
	public static final String LW_MYTEL_TURN_ASK_OTHERMSISDN_NOTVN = "lw.mytel.turn.ask.othermsisdn.notvn";

	public static final String LW_MYTEL_CHAT_MESSAGE_NOT_ACCEPTHELP_LABEL = "lw.mytel.chat.message.not.acceptHelp.label";
	public static final String LW_MYTEL_CHAT_MESSAGE_NOT_ACCEPTHELP_LINK = "lw.mytel.chat.message.not.acceptHelp.link";

	public static final String LW_MYTEL_CHAT_MESSAGE_OA_SMS_ALIAS = "lw.mytel.chat.message.oa.sms.alias";
	public static final String LW_MYTEL_CHAT_MESSAGE_OA_SECRET_INSTALL_SUCCESS = "lw.mytel.chat.message.oa.secret.install.success";

	public static final String LW_MYTEL_CHAT_MESSAGE_OA_SECRET_INSTALL_ACTIVATE = "lw.mytel.chat.message.oa.secret.install.activate";
	public static final String LW_MYTEL_CHAT_MESSAGE_OA_SECRET_INSTALL_DEVICE = "lw.mytel.chat.message.oa.secret.install.device";
	public static final String LW_MYTEL_CHAT_MESSAGE_OA_SECRET_INSTALL_NOT_SUM = "lw.mytel.chat.message.oa.secret.install.not.sum";

	public static final String LW_MYTEL_NOTE_POINT = "lw.mytel.note.point";
	public static final String LW_MYTEL_NOTE_POINT_10K = "lw.mytel.note.point.10k";
	public static final String LW_MYTEL_NOTE_10 = "lw.mytel.note.10";
	public static final String LW_MYTEL_NOTE_INSTALL = "lw.mytel.note.install";
	public static final String LW_MYTEL_NOTE_CARD = "lw.mytel.note.card";

	public static final String LW_MYTEL_CHAT_MESSAGE_OA_NEWS_SUCCESS = "lw.mytel.chat.message.oa.news.success";
	public static final String LW_MYTEL_NOTE_NEWS = "lw.mytel.note.news";

}
