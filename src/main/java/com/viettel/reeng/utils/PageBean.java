package com.viettel.reeng.utils;

import java.io.Serializable;

public class PageBean implements Serializable {

	private int count;// tong so ban ghi
	private int pageSize;// so bang ghi trong trang
	private int pageCount;// tong so trang
	private int page;// trang hien tai
	private String requestMapping;

	public PageBean() {
		count = 0;
		pageSize = 10;
		pageCount = 0;
		page = 1;
	}

	public PageBean(int count) {
		this.count = 0;
		this.pageSize = 10;
		this.pageCount = 0;
		this.page = 1;
		this.count = count;
	}

	public String getRequestMapping() {
		return requestMapping;
	}

	public void setRequestMapping(String requestMapping) {
		this.requestMapping = requestMapping;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		if (pageSize != 0) {
			pageCount = count / pageSize;
			if (count % pageSize != 0) {
				pageCount++;
			}
		}
		this.count = count;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page >= 1 ? page : 1;
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getOffset() {
		return (page - 1) * pageSize;
	}

}
