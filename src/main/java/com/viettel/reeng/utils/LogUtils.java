package com.viettel.reeng.utils;

import java.net.Inet4Address;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LogUtils {

	private static final Logger mochaUerAction = LogManager.getLogger("mochaUserAction");

	public static void mochaUserActionCdr(String msisdn, String action, int amount, String subAction, String info) {
		String server = "s";
		try {
			server = Inet4Address.getLocalHost().getHostName();
		} catch (Exception e) {
		}
		String key = ReengUtils.getFormatYyyyMMddHHmmss.get().format(new Date()) + "|" + msisdn + "|" + server + "|"
				+ action + "|" + amount + "|" + subAction + "|" + info;
		mochaUerAction.fatal(key);

	}
}
