package com.viettel.reeng.utils;

import com.google.gson.JsonObject;

public class HttpErrorCode {
	private int code;
	private String desc;

	public HttpErrorCode(HttpErrorCodeBuilder builder) {
		this.code = builder.code;
		this.desc = builder.desc;
	}

	public int getCode() {
		return code;
	}

	public String getDesc() {
		return desc;
	}

	public JsonObject toJson() {
		JsonObject obj = new JsonObject();
		obj.addProperty("code", this.code);
		obj.addProperty("desc", this.desc);
		obj.addProperty("errorCode", this.code);
		return obj;
	}

	@Override
	public String toString() {
		return toJson().toString();
	}

	public static class HttpErrorCodeBuilder {
		private int code;
		private String desc;

		public HttpErrorCodeBuilder code(int code) {
			this.code = code;
			return this;
		}

		public HttpErrorCodeBuilder desc(String desc) {
			this.desc = desc;
			return this;
		}

		public HttpErrorCode build() {
			return new HttpErrorCode(this);
		}
	}
}
