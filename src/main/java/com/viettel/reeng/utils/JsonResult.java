package com.viettel.reeng.utils;

import org.apache.commons.lang.StringUtils;

import com.google.gson.JsonObject;

public class JsonResult {
	private int code;
	private String code_;
	private String value;
	//
	private String countryCode;
	private String countryName;

	public String getCode_() {
		return code_;
	}

	public void setCode_(String code_) {
		this.code_ = code_;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public JsonResult(int code, String value) {
		this.code = code;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	@Override
	public String toString() {
		if (code_ != null && !("").equals(code_)) {
			JsonObject jobj = new JsonObject();
			jobj.addProperty("errorCode", getCode());
			jobj.addProperty("msisdn", getValue());
			jobj.addProperty("code", getCode_());
			if (StringUtils.isNotEmpty(getCountryCode())) {
				jobj.addProperty("countryCode", getCountryCode());
				jobj.addProperty("countryName", getCountryName());
			}
			return jobj.toString();
			// return "{\"errorCode\":" + getCode() + ",\"msisdn\":\"" + getValue() +
			// "\",\"code\":\"" + getCode_() + "\"}";
		} else {
			JsonObject jobj = new JsonObject();
			jobj.addProperty("errorCode", getCode());
			jobj.addProperty("desc", getValue());
			if (StringUtils.isNotEmpty(getCountryCode())) {
				jobj.addProperty("countryCode", getCountryCode());
				jobj.addProperty("countryName", getCountryName());
			}
			return jobj.toString();
			// return "{\"errorCode\":" + getCode() + ",\"desc\":\"" + getValue() + "\"}";
		}
	}

	public String toStringNew() {
		JsonObject jobj = new JsonObject();
		jobj.addProperty("code", getCode());
		jobj.addProperty("desc", getValue());
		return jobj.toString();
		// return "{\"code\":" + getCode() + ",\"desc\":\"" + getValue() + "\"}";
	}
}
