package com.viettel.reeng.utils;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

public interface IRowKey {
	public static final int DIGEST_BYTE_SIZE = 16;

	public byte[] makeRow() throws IOException, NoSuchAlgorithmException;

	public IRowKey readRow(byte[] row) throws IOException;
}
