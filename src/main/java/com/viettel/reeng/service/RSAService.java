package com.viettel.reeng.service;

import com.google.gson.Gson;
import com.viettel.reeng.config.Configuration;
import com.viettel.reeng.dto.JSecurityObj;
import com.viettel.reeng.utils.RSAEncryptUtil;
import com.viettel.reeng.utils.ReengCodeDesc.ReengCode;
import com.viettel.reeng.utils.ReengNotAuthorizedException;
import com.viettel.reeng.utils.ReengUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.xml.parsers.ParserConfigurationException;
import java.security.PrivateKey;

@Service
public class RSAService {

	private static final Logger logger = LogManager.getLogger(RSAService.class);
	@Autowired
	private Configuration config;
	private PrivateKey privateKey;

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(PrivateKey privateKey) {
		this.privateKey = privateKey;
	}

	@PostConstruct
	public void init() {
		try {
			String privateString = config.getSmsPrivateKeyPath();
			String key = RSAEncryptUtil.readFileAsString(privateString);
			key = key.replace("-----BEGIN PRIVATE KEY-----", "").replace("\n", "");
			// Remove the first and last lines
			key = key.replace("-----BEGIN PRIVATE KEY-----", "");
			privateKey = RSAEncryptUtil.getPrivateKeyFromString(key);
		} catch (ParserConfigurationException ex) {
			logger.error(ex);
		} catch (Exception e) {
			logger.error(e);
		}

	}

	public boolean validate2(String msisdn, String token, String md5String, String encryptedData) {
		String json_Value = decrypt(encryptedData);
		try {
			if (json_Value != null && json_Value.length() > 0) {

				Gson gson = new Gson();
				JSecurityObj secObj = gson.fromJson(json_Value, JSecurityObj.class);
				if (secObj.getMd5().equals(md5String) && secObj.getToken().equals(token)) {
					return true;
				}
				return false;
			}
		} catch (Exception ex) {
			logger.error("validate2 error: msisdn: " + msisdn + ",\n token: " + token + ",\n md5String = " + md5String
					+ ",\n encryptedData: " + encryptedData, ex);
		}
		return false;
	}

	public boolean validate2(String msisdn, String token, String md5String, JSecurityObj secObj) {
		try {
			if (secObj.getMd5().equals(md5String) && secObj.getToken().equals(token)) {
				return true;
			}
			return false;
		} catch (Exception ex) {
			logger.error("validate2 error: msisdn: " + msisdn + ",\n token: " + token + ",\n md5String = " + md5String,
					ex);
		}
		return false;
	}

	public JSecurityObj getObj(String encryptedData) {
		try {
			String json_Value = decrypt(encryptedData);
			if (StringUtils.isNotEmpty(json_Value)) {
				Gson gson = new Gson();
				JSecurityObj secObj = gson.fromJson(json_Value, JSecurityObj.class);
				return secObj;
			}
		} catch (Exception e) {
			logger.info("[Exception ]", e);
		}
		return null;
	}

	private String decrypt(String encryptedData) {
		try {
			return RSAEncryptUtil.decrypt(encryptedData.replaceAll(" ", ""), getPrivateKey());
		} catch (Exception ex) {
			logger.error("Decode data: [" + encryptedData + "] has error:" + ex);
			return "";
		}
	}
}
