package com.viettel.reeng.service;

import com.viettel.reeng.dao.sqlserver.NetNewsDao;
import com.viettel.reeng.entity.News;
import com.viettel.reeng.utils.NetNewsUtilsDB;
import com.viettel.reeng.utils.NetnewsUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class NetNewsService {

    @Autowired
    private NetNewsDao netNewsDao;
    private static final Logger logger = LogManager.getLogger(NetNewsService.class);

    @Autowired
    private NetNewsUtilsDB netNewsUtilsDB;

    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    //<editor-fold desc="getNewsHomeV2 ">
    public News getTopNews() {
        News news = netNewsDao.getTopNews();
        try {
            news.setReads(netNewsUtilsDB.getReads(news.getID()));
            news.setMedia_url(netNewsUtilsDB.getMedia(news.getID(), 12));
            news.setImage(netNewsUtilsDB.getMediaNews(news.getID(), 4));
            news.setImage169(netNewsUtilsDB.getMediaNews(news.getID(), 4));
            news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
            news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
            news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
            news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
            Date datePub = formatter.parse(news.getDatePub());
            news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));

            news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage169(), news.getID()));
            news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID()));
            news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), 0));
            news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
            news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
            news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
            news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
            news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
            news.setType(1);
            news.setPosition(0);
            news.setHeader("Tin top");
            news.setContent("");
            news.setCategoryName("");
            news.setLatestTitle("");
            news.setQuote("");
            news.setUrlOrigin("");
            news.setDateAlarm("");
            news.setPoster("");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return news;
    }

    public List<News> getFocusNews(int vip) {
        List<News> items = new ArrayList<News>();
        List<News> threeNews = netNewsDao.getTopThreeFocusNew();
        List<News> twentyFileNews = netNewsDao.getTopTwentyFiveFocusNew();
        if (CollectionUtils.isNotEmpty(threeNews)) {
            if (CollectionUtils.isNotEmpty(twentyFileNews))
                items = Stream.concat(threeNews.stream(), twentyFileNews.stream())
                        .collect(Collectors.toList());
            else
                items = threeNews;
        } else {
            if (CollectionUtils.isNotEmpty(twentyFileNews))
                items = twentyFileNews;
        }


        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setMedia_url(netNewsUtilsDB.getMedia(news.getID(), 12));
                    news.setImage169(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setImage(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));

                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setPosition(1);
                    news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage169(), news.getID(), vip));
                    news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID(), vip));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), vip));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setType(2);
                    news.setHeader("Tin tiêu điểm");
                    news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
                    news.setPoster("");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getTopEventNews() {
        List<News> items = netNewsDao.getTopEventNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setImage(netNewsUtilsDB.getImageEvent(news.getID(), 1) + netNewsUtilsDB.getImageEvent(news.getID(), 2));
                    news.setLatestTitle(netNewsUtilsDB.getLatestTitleOfCate(news.getID()));


                    news.setCid(Integer.parseInt(String.valueOf(news.getID())));
                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setPosition(5);
                    news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID()));
                    news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID()));
                    news.setContent("");
                    news.setShapo("");
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date date = new Date();
                    news.setDatePub(dateFormat.format(date));
                    news.setMedia_url("");
                    news.setDateAlarm("");
                    news.setSourceName("");
                    news.setPoster("");
                    news.setDuration("");
                    news.setType(2);
                    news.setCategory("Sự kiện");
                    news.setHeader("Tin sự kiện");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getTopInterestNews() {
        List<News> items = netNewsDao.getTopInterestNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));

                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setPosition(3);
                    news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage169(), news.getID()));
                    news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID()));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), 0));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setType(2);
                    news.setHeader("Tin được quan tâm");
                    news.setPoster("");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getTopQuoteNews() {
        List<News> items = netNewsDao.getTopQuoteNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));

                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setPosition(4);
                    news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage169(), news.getID()));
                    news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID()));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), 0));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setType(2);
                    news.setHeader("Câu trích dẫn");
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getTopVideoNews(int vip) {
        List<News> items = netNewsDao.getTopVideoNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setMedia_url(netNewsUtilsDB.getMedia(news.getID(), 15));
                    news.setImage169(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setImage(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));

                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setType(1);
                    news.setPosition(5);
                    news.setImage(NetnewsUtils.genRewriteURLImageEvent(news.getImage169(), news.getID()));
                    news.setImage169(NetnewsUtils.genRewriteURLImageEvent(news.getImage(), news.getID()));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), vip));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setHeader("NetTV");
                    news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getTopFiveVideoNews(int vip) {
        List<News> items = netNewsDao.getTopFiveVideoNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));

                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setType(2);
                    news.setPosition(5);
                    news.setImage(NetnewsUtils.genRewriteURLImageEvent(news.getImage169(), news.getID()));
                    news.setImage169(NetnewsUtils.genRewriteURLImageEvent(news.getImage(), news.getID()));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), vip));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setHeader("NetTV");
                    news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getVideoNews(int vip) {
        List<News> items = new ArrayList<News>();
        try {
            List<News> topVideo = getTopVideoNews(vip);
            List<News> topFiveVideo = getTopFiveVideoNews(vip);

            if (CollectionUtils.isNotEmpty(topVideo)) {
                if (CollectionUtils.isNotEmpty(topFiveVideo))
                    items = Stream.concat(topVideo.stream(), topFiveVideo.stream())
                            .collect(Collectors.toList());
                else
                    items = topVideo;
            } else {
                if (CollectionUtils.isNotEmpty(topFiveVideo))
                    items = topFiveVideo;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return items;
    }

    public List<News> getTopRadioNews(int vip) {
        List<News> items = netNewsDao.getTopRadioNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setMedia_url(netNewsUtilsDB.getMedia(news.getID(), 12));
                    news.setImage169(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setImage(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatTimerRadio(datePub));


                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setType(1);
                    news.setPosition(6);
                    news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage169(), news.getID(), vip));
                    news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID(), vip));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), vip));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setHeader("NetRadio");
                    news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getTopFourRadioNews(int vip) {
        List<News> items = netNewsDao.getTopFourRadioNews();
        try {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setReads(netNewsUtilsDB.getReads(news.getID()));
                    news.setImage169(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setImage(netNewsUtilsDB.getMediaNetnews(news.getID()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setDuration(netNewsUtilsDB.getIconTimer(news.getID()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatTimerRadio(datePub));


                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setType(2);
                    news.setPosition(6);
                    news.setImage(NetnewsUtils.genRewriteURLImage(news.getImage169(), news.getID(), vip));
                    news.setImage169(NetnewsUtils.genRewriteURLImage(news.getImage(), news.getID(), vip));
                    news.setContent("");
                    news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), vip));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setHeader("NetRadio");
                    news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return items;
    }

    public List<News> getRadioNews(int vip) {
        List<News> items = new ArrayList<News>();
        try {

            List<News> topRadio = getTopRadioNews(vip);
            List<News> topFourRadio = getTopFourRadioNews(vip);

            if (CollectionUtils.isNotEmpty(topRadio)) {
                if (CollectionUtils.isNotEmpty(topFourRadio))
                    items = Stream.concat(topRadio.stream(), topFourRadio.stream())
                            .collect(Collectors.toList());
                else
                    items = topRadio;
            } else {
                if (CollectionUtils.isNotEmpty(topFourRadio))
                    items = topFourRadio;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return items;
    }

    //</editor-fold>

    //<editor-fold desc="getListCateRelateEvent ">

    public List<News> getListCateRelateEvent(int pid,int cid,int id,int page)
    {
        List<News> items = netNewsDao.getListCateRelateEvent(pid,cid,id,page);
        try
        {
            if (CollectionUtils.isNotEmpty(items)) {
                for (News news : items) {
                    news.setImage(netNewsUtilsDB.getMedia(news.getID(),4));
                    news.setImage169(netNewsUtilsDB.getMediaIPAD(news.getID(),6));
                    news.setMedia_url(netNewsUtilsDB.getMedia(news.getID(), 12));
                    Date datePub = formatter.parse(news.getDatePub());
                    news.setDatePub(netNewsUtilsDB.formatDate3GApp(datePub));
                    news.setCategory(netNewsUtilsDB.getCategoryName(news.getPid()));
                    news.setSourceName(netNewsUtilsDB.getSourceNameApp(news.getSid()));
                    news.setTypeIcon(netNewsUtilsDB.getIconApp(news.getID()));


                    news.setUrl(NetnewsUtils.getTinBaiDetail(String.valueOf(news.getPid()), String.valueOf(news.getCid()), String.valueOf(news.getID()), news.getTitle()));
                    news.setType(1);
                    news.setPosition(5);
                    news.setImage(NetnewsUtils.genRewriteURLImageEvent(news.getImage169(), news.getID()));
                    news.setImage169(NetnewsUtils.genRewriteURLImageEvent(news.getImage(), news.getID()));
                    news.setContent("");
                    //news.setMedia_url(NetnewsUtils.getMediaUrl(news.getMedia_url(), news.getID(), vip));
                    news.setIsRead(news.getShapo().length() > 4 ? 1 : 0);
                    news.setSourceIcon(NetnewsUtils.getSourceAndIconName(news.getSourceName())[1]);
                    news.setSourceName(NetnewsUtils.getSourceAndIconName(news.getSourceName())[0]);
                    news.setDateAlarm("");
                    news.setHeader("NetTV");
                    news.setIs_nghe(news.getDuration().length() > 2 ? 1 : 0);
                }
            }

        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return  items;
    }
    //</editor-fold>

}
