package com.viettel.reeng.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisService {

	public static final String METFONE_REMAIN_KEY = "u:remain:%s";

	public static final String UNITEL_LAOS_REMAIN_KEY = "u:remain:%s";

	@Autowired
	@Qualifier("redisUICluster")
	private RedisTemplate<String, String> redisUICluster;

	public RedisTemplate<String, String> getRedisUICluster() {
		return redisUICluster;
	}

	public void setRedisUICluster(RedisTemplate<String, String> redisUICluster) {
		this.redisUICluster = redisUICluster;
	}

}
