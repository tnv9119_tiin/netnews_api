package com.viettel.reeng.entity;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class News {
    private int ID;
    private String Title;
    private int Pid;
    private int Cid;
    private String Url;
    private String Image;
    private String Image169;
    private String Shapo;
    private String Content;
    private String DatePub;
    private int Like;
    private int Comment;
    private int Reads;
    private String Media_url;
    private int type;
    private String Category;
    private String CategoryName;
    private int isRead;
    private int TypeIcon;
    private int cateevent ;
    private int idStory ;
    private String DateAlarm ;
    private String SourceName ;
    private String SourceIcon ;
    private String sid ;
    private int Position ;
    private String Header ;
    private String Poster ;
    private String Duration ;
    private long Timestamp ;
    private int is_nghe ;
    private List<Body> Body = new ArrayList<Body>();
    private String Quote ;
    private long unixTime ;
    private String LatestTitle ;
    private String UrlOrigin ;

    private  int Mid;//chinh la id1 trong bang status
    public int getMid() {
        return Mid;
    }

    public void setMid(int mid) {
        this.Mid = mid;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getReads() {
        return Reads;
    }

    public void setReads(int reads) {
        Reads = reads;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public int getPid() {
        return Pid;
    }

    public void setPid(int pid) {
        Pid = pid;
    }

    public int getCid() {
        return Cid;
    }

    public void setCid(int cid) {
        Cid = cid;
    }

    public String getUrl() {
        return Url;
    }

    public void setUrl(String url) {
        Url = url;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getImage169() {
        return Image169;
    }

    public void setImage169(String image169) {
        Image169 = image169;
    }

    public String getShapo() {
        return Shapo;
    }

    public void setShapo(String shapo) {
        Shapo = shapo;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public String getDatePub() {
        return DatePub;
    }

    public void setDatePub(String datePub) {
        DatePub = datePub;
    }

    public int getLike() {
        return Like;
    }

    public void setLike(int like) {
        Like = like;
    }

    public int getComment() {
        return Comment;
    }

    public void setComment(int comment) {
        Comment = comment;
    }



    public String getMedia_url() {
        return Media_url;
    }

    public void setMedia_url(String media_url) {
        Media_url = media_url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getTypeIcon() {
        return TypeIcon;
    }

    public void setTypeIcon(int typeIcon) {
        TypeIcon = typeIcon;
    }

    public int getCateevent() {
        return cateevent;
    }

    public void setCateevent(int cateevent) {
        this.cateevent = cateevent;
    }

    public int getIdStory() {
        return idStory;
    }

    public void setIdStory(int idStory) {
        this.idStory = idStory;
    }

    public String getDateAlarm() {
        return DateAlarm;
    }

    public void setDateAlarm(String dateAlarm) {
        DateAlarm = dateAlarm;
    }

    public String getSourceName() {
        return SourceName;
    }

    public void setSourceName(String sourceName) {
        SourceName = sourceName;
    }

    public String getSourceIcon() {
        return SourceIcon;
    }

    public void setSourceIcon(String sourceIcon) {
        SourceIcon = sourceIcon;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public int getPosition() {
        return Position;
    }

    public void setPosition(int position) {
        Position = position;
    }

    public String getHeader() {
        return Header;
    }

    public void setHeader(String header) {
        Header = header;
    }

    public String getPoster() {
        return Poster;
    }

    public void setPoster(String poster) {
        Poster = poster;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public int getIs_nghe() {
        return is_nghe;
    }

    public void setIs_nghe(int is_nghe) {
        this.is_nghe = is_nghe;
    }

    public List<com.viettel.reeng.entity.Body> getBody() {
        return Body;
    }

    public void setBody(List<com.viettel.reeng.entity.Body> body) {
        Body = body;
    }

    public String getQuote() {
        return Quote;
    }

    public void setQuote(String quote) {
        Quote = quote;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }

    public String getLatestTitle() {
        return LatestTitle;
    }

    public void setLatestTitle(String latestTitle) {
        LatestTitle = latestTitle;
    }

    public String getUrlOrigin() {
        return UrlOrigin;
    }

    public void setUrlOrigin(String urlOrigin) {
        UrlOrigin = urlOrigin;
    }
}
