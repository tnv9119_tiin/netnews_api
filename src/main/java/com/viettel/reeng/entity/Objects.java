package com.viettel.reeng.entity;

import com.viettel.reeng.config.Constants;
import com.viettel.reeng.utils.ReengUtils;
import com.viettel.reeng.utils.Utils;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import org.springframework.util.StringUtils;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

public class Objects {
    private int id = 0;
    private String title = "";
    private int pid = 0;
    private int cid = 0;
    private String url = "";
    private String image = "";
    private String image169 = "";
    private String shapo = "";
    private String content = "";
    private String datePub = "";
    private int like = 0;
    private int comment = 0;
    private int reads = 0;
    private String media_url = "";
    private int type = 0;
    private String category = "";
    private String categoryName = "";
    private int isRead = 0;
    private int typeIcon = 0;
    private int cateEvent = 0;
    private int idStory = 0;
    private String dateAlarm = "";
    private String sourceIcon ="";
    private String sourceName ="";
    private String sid = "";
    private int position = 0;
    private String header = "";
    private String poster = "";
    private String duration = "";



    private long timeStamp = 0;
    private int is_nghe = 0;
    public List<Body> body = new ArrayList<Body>();
    private String quote = "";
    private long unixTime = 0;
    private String lastestTitle = "";
    private String urlOrigin = "";
    private String source = "";

    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public Objects() {
        this.id = 0;
        this.title = "";
        this.pid = 0;
        this.cid = 0;
        this.url = "";
        this.image = "";
        this.image169 = "";
        this.shapo = "";
        this.content = "";
        this.datePub = "";
        this.like = 0;
        this.comment = 0;
        this.reads = 0;
        this.media_url = "";
        this.type = 0;
        this.category = "";
        this.categoryName = "";
        this.isRead = 0;
        this.typeIcon = 0;
        this.cateEvent = 0;
        this.idStory = 0;
        this.dateAlarm = "";
        this.sourceIcon ="";
        this.sourceName ="";
        this.sid = "";
        this.position = 0;
        this.header = "";
        this.poster = "";
        this.duration = "";



        this.timeStamp = 0;
        this.is_nghe = 0;
        this.body = new ArrayList<>();
        this.quote = "";
        this.unixTime = 0;
        this.lastestTitle = "";
        this.urlOrigin = "";
        this.source = "";
    }

    public Objects(int id, int pid, int cid, String title, String content, String shapo, String datePub, String image,
                   String CategoryName, String SourceName )
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;


        String sMsgDateListFormat = "";

        try {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datePub);
            sMsgDateListFormat = Utils.VNFormatDate(date);
            sMsgDateListFormat = "<p class=\"ndl-datetime\"><span>" + sMsgDateListFormat + "</span></p>";
        }  catch (Exception e){

        }

        this.content = sMsgDateListFormat + content + "<p class=\"source-detail\">" + SourceName + "</p>";
        this.category = CategoryName; ;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = 1;
        this.comment = 1;
        this.reads = 1;
        this.image = image;
        this.media_url = "";

        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
    }

    public Objects(int id, int pid, int cid, String title, String content, String shapo, String datePub, String image,
                   String CategoryName, String SourceName, String sid)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        // this.image = image;


        String sMsgDateListFormat = "";

        try
        {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datePub);
            sMsgDateListFormat = Utils.VNFormatDate(date);
            sMsgDateListFormat = "<p class=\"ndl-datetime\"><span>" + sMsgDateListFormat + "</span></p>";
        }  catch (Exception e){

        }

        this.content = sMsgDateListFormat + content + "<p class=\"source-detail\">" + SourceName + "</p>";
        this.category = CategoryName;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = 1;
        this.comment = 1;
        this.reads = 1;
        this.image = image;
        this.media_url = "";

        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.sid = sid;
    }


    public Objects(int id, int pid, int cid, String title, String content, String shapo, String datePub, int like,
                   int comment, int reads, String categoryName, String longid)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        // this.image = image;


        String sMsgDateListFormat = "";

        try
        {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datePub);
            sMsgDateListFormat = Utils.VNFormatDate(date);
            sMsgDateListFormat = "<p class=\"ndl-datetime\"><span>" + sMsgDateListFormat + "</span></p>";
        }  catch (Exception e){

        }
        String sWebImage = Constants.ImageDBPath;
        if (!longid.equals("0"))
        {
            String sMsgContent = "";

            sMsgContent = sMsgDateListFormat + content;
            sMsgContent += "<p style=\"text-align: center;\">";
            sMsgContent += " <a href=\"http://m.netnews.vn/ViewNew.aspx?id=" + longid + "&amp;pid=" + pid + "&amp;cid=" + cid;
            sMsgContent += " \"><span style=\"font-family: Times New Roman;\">&gt;&gt;Xem tiếp</span></a></p>";

            this.content = sMsgContent;
        }
        else
        {
            this.content = sMsgDateListFormat + content;
        }
        this.category = categoryName;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;

        this.media_url = "";

        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
    }



    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, String pName, String cName, String image169, String typeIcon)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }



        this.type = 2;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }

        this.typeIcon = Integer.valueOf(typeIcon);

    }
    //Hàm tạo nguồn
    public Objects(int id, String title, String image, String shapo, String datePub,  int type, String image169,
                   String link, String sid, String sourceName)
    {
        this.id = id;
        this.url = link;
        this.title = title;
        if (image.indexOf("http") != -1)
        {
            this.image = GenRewriteURLImage(image169,id);
            this.image169 = GenRewriteURLImage(image,id);
        }
        else
        {
            this.image = Constants.ImageTinTopPath + image169;
            this.image169 = Constants.ImageTinTopPath + image;
        }
        this.shapo = shapo;
        this.datePub = datePub;
        this.type = type;
        this.sid = sid;
        this.sourceName = sourceName;

    }

    public Objects(int id, String title, String image, String shapo, String datePub, int type, String image169,
                   String link, String sid, String sourceName,int vip)
    {
        this.id = id;
        this.url = link;
        this.title = title;
        if (image.indexOf("http") != -1)
        {
            this.image = GenRewriteURLImage(image169, id,vip);
            this.image169 = GenRewriteURLImage(image, id, vip);
        }
        else
        {
            this.image = Constants.ImageTinTopPath + image169;
            this.image169 = Constants.ImageTinTopPath + image;
        }
        this.shapo = shapo;
        this.datePub = datePub;
        this.type = type;
        this.sid = sid;
        this.sourceName = sourceName;

    }
    //Hàm tạo radio
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName, String image169,
                   long timestamp, String duration, String sourceName, String sid)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.timeStamp = timestamp;
        this.duration = duration;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
    }
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName, String image169,
                   long timestamp, String duration, String sourceName, String sid, int vip)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, vip);
        this.image169 = GenRewriteURLImage(image, id, vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.timeStamp = timestamp;
        this.unixTime = timestamp;
        this.duration = duration;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName, String image169,
                   long timestamp, String duration, String sourceName, String sid, int vip, String urlOrigin)
    {
        this.urlOrigin = urlOrigin;
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, vip);
        this.image169 = GenRewriteURLImage(image, id, vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.timeStamp = timestamp;
        this.unixTime = timestamp;
        this.duration = duration;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName, String image169,
                   long timestamp, String duration, String sourceName, String sid, int vip, int position)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, vip);
        this.image169 = GenRewriteURLImage(image, id, vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.timeStamp = timestamp;
        this.unixTime = timestamp;
        this.duration = duration;
        this.sid = sid;
        if ((sourceName.contains(",")) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.position = position;
    }

    // ham tao radio cu
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName, String image169,
                   long timestamp, String duration, String urlOrigin)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        if (cid == 444)
        {
            this.image = GenRewriteURLImageStory(image169, id);
            this.image169 = GenRewriteURLImageStory(image, id);
        }
        else
        {
            this.image = GenRewriteURLImage(image169, id);
            this.image169 = GenRewriteURLImage(image, id);
        }
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.timeStamp = timestamp;
        this.unixTime = timestamp;
        this.duration = duration;
        this.urlOrigin = urlOrigin;

    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName, String image169, String duration)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        if (cid == 444)
        {
            this.image = GenRewriteURLImageStory(image169, id);
            this.image169 = GenRewriteURLImageStory(image, id);
        }
        else
        {
            this.image = GenRewriteURLImage(image169, id);
            this.image169 = GenRewriteURLImage(image, id);
        }
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }

        this.duration = duration;

    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int position, String duration)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;

        this.image = GenRewriteURLImage(image169, id);
        this.image169 = GenRewriteURLImage(image, id);

        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.position = position;
        this.duration = duration;

    }


    //Ham tao trang chu app moi 2017/09/06
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   String iconradio, String typeIcon, int vip)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, vip);
        this.image169 = GenRewriteURLImage(image, id, vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.body = itemBody;
        if (iconradio.length() > 1)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   String iconradio, String typeIcon, int vip, long UnixTime)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, vip);
        this.image169 = GenRewriteURLImage(image, id, vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.body = itemBody;
        if (iconradio.length() > 1)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.unixTime = UnixTime;
    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   String iconradio, String typeIcon, int vip, long UnixTime,String buf,long timestamp)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, vip);
        this.image169 = GenRewriteURLImage(image, id, vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.body = itemBody;
        if (iconradio.length() > 1)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.unixTime = UnixTime;
        this.timeStamp = timestamp;
    }


    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   String iconradio, String typeIcon)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.body = itemBody;
        if (iconradio.length() > 1)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
    }

    // API su kien
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   String iconradio, String typeIcon, String cateName)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.body = itemBody;
        if (iconradio.length() > 1)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.categoryName = cateName;
    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   String iconradio, String typeIcon, String cateName,int isVip)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id, isVip);
        this.image169 = GenRewriteURLImage(image, id, isVip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, isVip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, isVip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

        this.body = itemBody;
        if (iconradio.length() > 1)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.categoryName = cateName;
    }
    //Ham tao trang chu app moi 2017/09/07

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   int position, String header, String poster, String iconradio, String typeIcon, int vip)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;

        if (position == 5)
        {
            this.image = GenRewriteURLImageEvent(image169, id);
            this.image169 = GenRewriteURLImageEvent(image, id);
        }
        else
        {
            this.image = GenRewriteURLImage(image169, id, vip);
            this.image169 = GenRewriteURLImage(image, id, vip);
        }
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
        this.body = itemBody;
        this.position = position;
        this.header = header;
        this.poster = poster;
        if (iconradio.length() > 2)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }

    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   int position, String header, String poster, String iconradio, String typeIcon, int vip,String latestTitle)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.lastestTitle = latestTitle;
        if (position == 5)
        {
            this.image = GenRewriteURLImageEvent(image169, id);
            this.image169 = GenRewriteURLImageEvent(image, id);
        }
        else
        {
            this.image = GenRewriteURLImage(image169, id, vip);
            this.image169 = GenRewriteURLImage(image, id, vip);
        }
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        this.isRead = shapo.length() > 4 ? 1 : 0;
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if (sourceName.contains(",") && (sourceName.length() > 40))
        {
            String[] sources = sourceName.split(",");

            this.sourceName = sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
        this.body = itemBody;
        this.position = position;
        this.header = header;
        this.poster = poster;
        if (iconradio.length() > 2)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }

    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   int position, String header, String poster, String iconradio, String typeIcon, int vip, long unixTime)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;

        if (position == 5)
        {
            this.image = GenRewriteURLImageEvent(image169, id);
            this.image169 = GenRewriteURLImageEvent(image, id);
        }
        else
        {
            this.image = GenRewriteURLImage(image169, id, vip);
            this.image169 = GenRewriteURLImage(image, id, vip);
        }
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id, vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
        this.body = itemBody;
        this.position = position;
        this.header = header;
        this.poster = poster;
        if (iconradio.length() > 2)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.unixTime = unixTime;
    }


    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   int position, String header, String poster, String iconradio, String typeIcon)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;

        if (position == 5)
        {
            this.image = GenRewriteURLImageEvent(image169, id);
            this.image169 = GenRewriteURLImageEvent(image, id);
        }
        else
        {
            this.image = GenRewriteURLImage(image169, id);
            this.image169 = GenRewriteURLImage(image, id);
        }
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
        this.body = itemBody;
        this.position = position;
        this.header = header;
        this.poster = poster;
        if (iconradio.length() > 2)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }

    }

    //Ham tao trang chu app moi 2017/09/07
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   int position, String header, String poster, String iconradio, String typeIcon, String quote)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
        this.body = itemBody;
        this.position = position;
        this.header = header;
        this.poster = poster;
        if (iconradio.length() > 2)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.quote = quote;
    }


    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, String sid, String sourceName, List<Body> itemBody,
                   int position, String header, String poster, String iconradio, String typeIcon, String quote,int vip)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169, id,vip);
        this.image169 = GenRewriteURLImage(image, id,vip);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url, id,vip);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url, id, vip);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.sid = sid;
        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }
        this.body = itemBody;
        this.position = position;
        this.header = header;
        this.poster = poster;
        if (iconradio.length() > 2)
            this.is_nghe = 1;
        this.duration = iconradio;
        if (!StringUtils.isEmpty(typeIcon)) {
            this.typeIcon = Integer.valueOf(typeIcon);
        }
        this.quote = quote;
    }

    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, String dateAlarm, List<Body> listBody)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.dateAlarm = dateAlarm;
        this.body = listBody;
    }



    //trang chu radio
    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url, int type, String pName, String cName,
                   String image169, int eventid, int idstory, String sourceName)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image169,id);
        this.image169 = GenRewriteURLImage(image,id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImage(media_url,id);
        }
        else if (media_url.toLowerCase().indexOf(".mp4") > 0)
        {
            this.media_url = GenRewriteUrlVideo(media_url,id,0);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
        this.cateEvent = eventid;
        this.idStory = idstory;

        if ((sourceName.indexOf(",") != -1) && (sourceName.length() > 40))
        {
            String[] Sources = sourceName.split(",");

            this.sourceName = Sources[0];
            this.sourceIcon = Constants.ImageDBPathVIP + Sources[1];
        }
        else
        {
            this.sourceName = sourceName;
        }

    }


    public Objects(int id, int pid, int cid, String title, String image, String content, String shapo, String datePub,
                   int like, int comment, int reads, String media_url)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiDetail(String.valueOf(pid), String.valueOf(cid), String.valueOf(id), title);
        this.title = title;
        this.image = GenRewriteURLImage(image,id);


        String sMsgDateListFormat = "";

        try
        {
            Date date = new SimpleDateFormat("yyyy-MM-dd").parse(datePub);
            sMsgDateListFormat = Utils.VNFormatDate(date);
            sMsgDateListFormat = "<p class=\"ndl-datetime\"><span>" + sMsgDateListFormat + "</span></p>";
        } catch (Exception e){

        }
        String sWebImage = Constants.ImageDBPath;
        this.content = sMsgDateListFormat + "<div class=\"news-content\">" + sWebImage + shapo + "</div>";

        //this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;

        this.media_url = "";

        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
    }

    public Objects(int id, int pid, int cid, String title, String image, String image169, String content, String shapo,
                   String datePub, int like, int comment, int reads, String media_url, int type, String cName)
    {
        this.id = id;
        this.pid = pid;
        this.cid = cid;
        this.url = GetTinBaiEvent(String.valueOf(pid), String.valueOf(cid));
        this.title = title;
        this.image = GenRewriteURLImageEvent(image169,id);
        this.image169 = GenRewriteURLImageEvent(image, id);
        this.content = content;
        this.shapo = shapo;
        this.datePub = datePub;
        this.like = like;
        this.comment = comment;
        this.reads = reads;
        if (media_url.toLowerCase().indexOf(".mp3") > 0)
        {
            this.media_url = GenRewriteURLImageEvent(media_url, id);
        }
        else
        {
            this.media_url = "";
        }

        this.type = type;
        this.category = cName;
        if (shapo.length() > 4)
        {
            this.isRead = 1;
        }
        else
        {
            this.isRead = 0;
        }
    }


    public static String GetTinBaiDetail(String parentID, String cid, String ID, String title)
    {
        try
        {
            if (Constants.isVip == "0")
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
            else
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
        }
        catch (Exception e)
        {
            if (Constants.isVip == "0")
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
            else
            {
                return Constants.LinkWap + getNiceUrl(title) + getAliasCM(parentID) + "-" + parentID + "-" + cid + "-" + ID + ".html";
            }
        }
    }


    protected static String getAliasCM(String pid)
    {
        if (pid == "0")
            return "-Home";
        if (pid == "1")
            return "-xa-hoi";
        if (pid == "3")
            return "-giai-tri";
        if (pid == "5")
            return "-phap-luat";
        if (pid == "6")
            return "-kinh-doanh";
        if (pid == "7")
            return "-nguoi-dep";
        if (pid == "10")
            return "-the-thao";
        if (pid == "29")
            return "-chuyen-la";
        if (pid == "135")
            return "-Video";
        if (pid == "149")
            return "-Cmcuoi";
        if (pid == "150")
            return "-quan-su";
        if (pid == "151")
            return "-audio";
        if (pid == "166")
            return "-thong-tin-dich-vu";
        if (pid == "194")
            return "-toan-canh-su-kien";
        if (pid == "280")
            return "-nong";
        if (pid == "312")
            return "-thien-nguyen";
        if (pid == "482")
            return "-tien-ich";
        if (pid == "542")
            return "-loi-xin-loi-muon";
        if (pid == "560")
            return "-tam-su";
        if (pid == "1488")
            return "-the-gioi";
        if (pid == "728")
            return "-nha-nong";
        if (pid == "313")
            return "-cong-nghe";
        if (pid == "416")
            return "-Xe";
        if (pid == "1540")
            return "-du-lich";
        return "-home";
    }


    public static String GetTinBaiEvent(String parentID, String cid)
    {
        try
        {
            return  "event-" + parentID + "-" + cid +  ".html";
        }
        catch (Exception e)
        {
            return "event-" + parentID + "-" + cid + ".html";
        }
    }
    /// <summary>
    /// NamNN crate
    /// </summary>
    /// <param name="sCharacter"></param>
    /// <returns></returns>
    public static String ConvertENRegex(String sCharacter)
    {
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        String temp = Normalizer.normalize(sCharacter, Normalizer.Form.NFD);
        return pattern.matcher(temp).replaceAll("").replaceAll("đ", "d").replaceAll("Đ", "D");
    }
    /// <summary>
    /// NamNN Edit
    /// </summary>
    /// <param name="objurl"></param>
    /// <returns></returns>
    public static String getNiceUrl(Object objurl)
    {
        try
        {
            String url = objurl.toString();
            String niceurl = ConvertENRegex(url);
            //Replace những kí tự không phải 0-9, a-z, A-Z bằng khoảng trắng
            niceurl = niceurl.replaceAll("[^0-9a-zA-Z]+", " ");
            //Replace khoảng trắng bằng dấu "-"
            niceurl = niceurl.replaceAll("\\s+", "-");

            //niceurl = removeChar(niceurl, new String[] { "/", "m²", ":", ",", "<", ">", "”", "“", ".", "!", "?", "@", "#", "$", "%", "^", "&", "*", "(", ")", "+", "~", "`", "\"" });
            //return niceurl;
            int indexLast = niceurl.lastIndexOf("-");
            //trường hợp có ký tự đặc biệt ở cuối title ==> sẽ thừa 1 dấu -
            if (indexLast == niceurl.length() - 1 && niceurl.length() > 0)
                niceurl = niceurl.substring(0, niceurl.length() - 1);
            return niceurl;
        }
        catch (Exception e)
        {
            return "";
        }
    }

    public static String ConvertVietnameseCharacterToEN(String sCharacter)
    {
        String sTemplate = "ĂẮẰẲẴẶăắằẳẵặÂẤẦẨẪẬâấầẩẫậÁÀẢÃẠáàảãạÔỐỒỔỖỘôốồổỗộƠỚỜỞỠỢơớờởỡợÓÒỎÕỌóòỏõọĐđÊẾỀỂỄỆêếềểễệÉÈẺẼẸéèẻẽẹƯỨỪỬỮỰưứừửữựÚÙỦŨỤúùủũụÍÌỈĨỊíìỉĩịÝỲỶỸỴýỳỷỹỵ";
        String sReplate = "AAAAAAaaaaaaAAAAAAaaaaaaAAAAAaaaaaOOOOOOooooooOOOOOOooooooOOOOOoooooDdEEEEEEeeeeeeEEEEEeeeeeUUUUUUuuuuuuUUUUUuuuuuIIIIIiiiiiYYYYYyyyyy";
        char[] arrChar = sTemplate.toCharArray();
        char[] arrReChar = sReplate.toCharArray();
        String sResult = "";//sCharacter;
        char digit;

        for (int ch = 0; ch < sCharacter.length(); ch++)
        {
            digit = sCharacter.charAt(ch);
            for (int i = 0; i < arrChar.length; i++)
                if (digit == arrChar[i])
                    digit = arrReChar[i];
            sResult += digit;
        }

        return sResult;
    }

    public static String GenRewriteURLImage(String lead_image, int id)
    {
        String link_image = "";

        try
        {
            if (lead_image.indexOf("http") != -1)
            {
                link_image = lead_image;
            }
            else
            {
                if (id == 0)
                {
                    link_image = Utils.sWebImageVip + lead_image;
                }
                else
                {
                    if (id >= Utils.Max_id_img_old)
                        link_image = Utils.sWebImageVipNew  + lead_image;
                    else
                        link_image = Utils.sWebImageVip + lead_image;
                }
            }
        }
        catch (Exception e)
        {

        }

        return link_image;
    }

    public static String GenRewriteURLImageStory(String lead_image, int id)
    {
        String link_image = "";

        try
        {
            if (lead_image.indexOf("http") != -1)
            {
                link_image = lead_image;
            }
            else
            {
                if (id == 0)
                {
                    link_image = Utils.sWebImageVip + lead_image;
                }
                else
                {
                    if (id >= 355)
                        link_image = Utils.sWebImageVipNew + lead_image;
                    else
                        link_image = Utils.sWebImageVip + lead_image;
                }
            }
        }
        catch (Exception e)
        {

        }

        return link_image;
    }


    public static String GenRewriteURLImage(String lead_image, int id, int vip)
    {
        String link_image = "";

        try
        {
            if (lead_image.toLowerCase().contains("http"))
            {
                link_image = lead_image;
            }
            else
            {
                if (vip == 1)
                {
                    if (id == 0)
                    {
                        link_image = Utils.sWebImageVip + lead_image;
                    }
                    else
                    {
                        if (id >= Utils.Max_id_img_old)
                            link_image = Utils.sWebImageVipNew + lead_image;
                        else
                            link_image = Utils.sWebImageVip + lead_image;
                    }
                }
                else
                {
                    if (id == 0)
                    {
                        link_image = Utils.sWebImage + lead_image;
                    }
                    else
                    {
                        if (id >= Utils.Max_id_img_old)
                            link_image = Utils.sWebImageNew + lead_image;
                        else
                            link_image = Utils.sWebImage + lead_image;
                    }
                }
            }
        }
        catch (Exception e)
        {

        }

        return link_image;
    }

    public static String GenRewriteUrlVideo(String leadImage, int id, int vip)
    {
        String link_image = "";

        try
        {
            if (leadImage.toLowerCase().indexOf("http") != -1)
            {
                link_image = leadImage;
            }
            else
            {
                int configCdn = Integer.parseInt(Constants.video_config_cdn);
                if (configCdn > 0 && configCdn <= id && leadImage.contains(".mp4"))
                {
                    if (vip == 1)
                        link_image = Constants.video_cdn + leadImage;
                    else
                        link_image = Constants.video_cdn_normal + leadImage;
                }
                else
                {
                    if (vip == 1)
                    {
                        if (id == 0)
                        {
                            link_image = Utils.sWebImageVip + leadImage;
                        }
                        else
                        {
                            if (id >= Utils.Max_id_img_old)
                                link_image = Utils.sWebImageVipNew + leadImage;
                            else
                                link_image = Utils.sWebImageVip + leadImage;
                        }
                    }
                    else
                    {
                        if (id == 0)
                        {
                            link_image = Utils.sWebImage + leadImage;
                        }
                        else
                        {
                            if (id >= Utils.Max_id_img_old)
                                link_image = Utils.sWebImageNew + leadImage;
                            else
                                link_image = Utils.sWebImage + leadImage;
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            //
        }

        return link_image;
    }

    public static String GenRewriteURLImageEvent(String lead_image, int id)
    {
        String link_image = "";

        try
        {

            link_image = lead_image.replace("archive/imageslead/", Utils.sWebImageVipNew + "archive/imageslead/");
            link_image = link_image.replace("/http://", "http://");

        }
        catch (Exception e)
        {

        }

        return link_image;
    }


    public static String removeChar(String niceurl, String[] danhsach)
    {
        for (String xoa : danhsach)
        {
            niceurl = niceurl.replace(xoa, "");
        }
        return niceurl;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage169() {
        return image169;
    }

    public void setImage169(String image169) {
        this.image169 = image169;
    }

    public String getShapo() {
        return shapo;
    }

    public void setShapo(String shapo) {
        this.shapo = shapo;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDatePub() {
        return datePub;
    }

    public void setDatePub(String datePub) {
        this.datePub = datePub;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    public int getComment() {
        return comment;
    }

    public void setComment(int comment) {
        this.comment = comment;
    }

    public int getReads() {
        return reads;
    }

    public void setReads(int reads) {
        this.reads = reads;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getIsRead() {
        return isRead;
    }

    public void setIsRead(int isRead) {
        this.isRead = isRead;
    }

    public int getTypeIcon() {
        return typeIcon;
    }

    public void setTypeIcon(int typeIcon) {
        this.typeIcon = typeIcon;
    }

    public int getCateEvent() {
        return cateEvent;
    }

    public void setCateEvent(int cateEvent) {
        this.cateEvent = cateEvent;
    }

    public int getIdStory() {
        return idStory;
    }

    public void setIdStory(int idStory) {
        this.idStory = idStory;
    }

    public String getDateAlarm() {
        return dateAlarm;
    }

    public void setDateAlarm(String dateAlarm) {
        this.dateAlarm = dateAlarm;
    }

    public String getSourceIcon() {
        return sourceIcon;
    }

    public void setSourceIcon(String sourceIcon) {
        this.sourceIcon = sourceIcon;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public int getIs_nghe() {
        return is_nghe;
    }

    public void setIs_nghe(int is_nghe) {
        this.is_nghe = is_nghe;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(String quote) {
        this.quote = quote;
    }

    public long getUnixTime() {
        return unixTime;
    }

    public void setUnixTime(long unixTime) {
        this.unixTime = unixTime;
    }

    public String getLastestTitle() {
        return lastestTitle;
    }

    public void setLastestTitle(String lastestTitle) {
        this.lastestTitle = lastestTitle;
    }

    public String getUrlOrigin() {
        return urlOrigin;
    }

    public void setUrlOrigin(String urlOrigin) {
        this.urlOrigin = urlOrigin;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public List<Body> getBody() {
        return body;
    }

    public void setBody(List<Body> body) {
        this.body = body;
    }
}

