package com.viettel.reeng.entity;

public class AbsModel {
    public errorInfo error;

    public AbsModel()
    {
    }

    public errorInfo getError() {
        return error;
    }

    public void setError(errorInfo error) {
        this.error = error;
    }
}
