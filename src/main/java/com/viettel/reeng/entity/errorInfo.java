package com.viettel.reeng.entity;

public class errorInfo {
    public String Message;
    public String Code;
    public String Excep;

    public errorInfo(String message, String code,String excep)
    {
        this.Message = message;
        this.Code = code;
        this.Excep = excep;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getExcep() {
        return Excep;
    }

    public void setExcep(String excep) {
        Excep = excep;
    }
}
