package com.viettel.reeng.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class Configuration {

	@Value("#{config['sec.sha.key']}")
	private String secSHAKey;

	@Value("#{config['smsout.privatekey.filepath']}")
	private String smsPrivateKeyPath;

	public String getSecSHAKey() {
		return secSHAKey;
	}

	public void setSecSHAKey(String secSHAKey) {
		this.secSHAKey = secSHAKey;
	}

	public String getSmsPrivateKeyPath() {
		return smsPrivateKeyPath;
	}

	public void setSmsPrivateKeyPath(String smsPrivateKeyPath) {
		this.smsPrivateKeyPath = smsPrivateKeyPath;
	}

}
