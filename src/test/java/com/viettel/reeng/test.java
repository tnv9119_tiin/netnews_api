/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.reeng;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.TemporalAdjusters;
import java.time.temporal.WeekFields;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import com.viettel.reeng.utils.ReengUtils;

/**
 *
 * @author thanhtx2
 */
public class test {
	public static final ThreadLocal<SimpleDateFormat> isodate = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		}
	};
		
	public static void main(String[] args) {
		try {



			// SERVER|S_MD5:3e4c2daff5ed167f4df9d132bde1d2b4|S_Token:33244337861541315435376290 
			// CLIENT|C_MD5:b62d4fbe0ffea1b176f9c445f1ba8dc5|C_Token:33244337861541315435376290|S_INPUT_MD5:09618370755101ios10960332443378615413154353762901542821358525 
			
			String md5String = ReengUtils.encryptMD5("09618370755101ios10960332443378615413154353762901542821358525");
			 
			System.out.println("==================="+md5String);
			 
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static LocalDate getFirstDay() {
		Locale locale = Locale.FRANCE;
		ZoneId TZ = ZoneId.of("Asia/Ho_Chi_Minh");
		DayOfWeek firstDayOfWeek = WeekFields.of(locale).getFirstDayOfWeek();
		return LocalDate.now(TZ).with(TemporalAdjusters.previousOrSame(firstDayOfWeek));
	}

	public static LocalDate getLastDay() {
		Locale locale = Locale.FRANCE;
		ZoneId TZ = ZoneId.of("Asia/Ho_Chi_Minh");
		DayOfWeek firstDayOfWeek = WeekFields.of(locale).getFirstDayOfWeek();
		DayOfWeek lastDayOfWeek = DayOfWeek.of(((firstDayOfWeek.getValue() + 5) % DayOfWeek.values().length) + 1);
		return LocalDate.now(TZ).with(TemporalAdjusters.nextOrSame(lastDayOfWeek));
	}

	private static long fakeStartTime() {
		// fake starttime
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 3);
		cal.set(Calendar.HOUR_OF_DAY, 15);
		cal.set(Calendar.MINUTE, 00);
		cal.set(Calendar.SECOND, 00);
		long startTime = cal.getTimeInMillis();
		return startTime;
	}

	private static String getStartTimeStr(long time) {
		Date d = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat("E, d/M");
		String ret = sdf.format(d);
		if (ret.contains("Sun")) {
			ret = ret.replaceAll("Sun", "CHỦ NHẬT");
		} else {
			ret = ret.replaceAll("Mon", "2").replaceAll("Tue", "3").replaceAll("Web", "4").replaceAll("Thu", "5")
					.replaceAll("Fri", "6").replaceAll("Sat", "7");
			ret = "THỨ " + ret;
		}

		return ret;
	}

	public class InforTemp {
		String error_code;
		String desc;
		lst_template lst_template;

		public InforTemp() {

		}

		public String getError_code() {
			return error_code;
		}

		public void setError_code(String error_code) {
			this.error_code = error_code;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public lst_template getLst_template() {
			return lst_template;
		}

		public void setLst_template(lst_template lst_template) {
			this.lst_template = lst_template;
		}

	}

	public class objectsInfo {
		int id;
		String name;
		String avatar;
		String preview;
		String path;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAvatar() {
			return avatar;
		}

		public void setAvatar(String avatar) {
			this.avatar = avatar;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getPreview() {
			return preview;
		}

		public void setPreview(String preview) {
			this.preview = preview;
		}

	}

	public class lst_template {

		String name;
		String discripsion;
		List<objectsInfo> objects_info;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDiscripsion() {
			return discripsion;
		}

		public void setDiscripsion(String discripsion) {
			this.discripsion = discripsion;
		}

		public List<objectsInfo> getObjects_info() {
			return objects_info;
		}

		public void setObjects_info(List<objectsInfo> objects_info) {
			this.objects_info = objects_info;
		}

	}
}
